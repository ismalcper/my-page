import styles from "../../styles/day.module.css";
import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

export default function Childrens() {
  const [position, setPosition] = useState({ x: -1000, y: 0 });
  const [withSize, setWithSize] = useState(0);

  const ref = useRef(null);
  useEffect(() => {
    setWithSize(ref.current ? ref.current.offsetWidth : 0);
  }, [ref.current]);
  useEffect(() => {
    addEventListeners();
    return () => removeEventListeners();
  }, []);
  const router = useRouter();
  const { name } = router.query;

  const addEventListeners = () => {
    document.addEventListener("mousemove", onMouseMove);
    document.addEventListener("touchmove", onTouchMove);
  };

  const removeEventListeners = () => {
    document.removeEventListener("mousemove", onMouseMove);
    document.removeEventListener("touchmove", onTouchMove);
  };

  const onMouseMove = (e) => {
    setPosition({ x: e.clientX, y: e.clientY });
  };

  const onTouchMove = (e) => {
    setPosition({
      x: e.targetTouches[0].clientX,
      y: e.targetTouches[0].clientY,
    });
  };

  return (
    <div className={`${styles.top} ${styles.body}`}>
      <div className={`${styles.scene}`}>
        <h1>¡Touch here! 🤭</h1>
        <h1>
          <b>{name}</b>
        </h1>
        {/* <FontAwesomeIcon
          icon={faSearch}
          size={"lg"}
          style={{ color: "#a4b0be" }}
        /> */}

        <br></br>

        <small style={{ color: "white" }}>
          Slide your finger across the screen and discover the image!
        </small>
        <br></br>

        <div
          ref={ref}
          className={`${styles.magic}`}
          style={{
            left: `${position.x - withSize / 2}px`,
            top: `${position.y - withSize / 2}px`,
          }}
        ></div>
      </div>
    </div>
  );
}

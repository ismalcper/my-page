import Head from "next/head";

import styles from "../styles/Home.module.css";
import Skill from "../components/skill";
import Pie from "../components/pie";
import Frase from "../components/frase";
import Proyectos from "../components/proyectos";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import {
  faFacebook,
  faInstagram,
  faWhatsapp,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons"; // import the icons you need

export default function Home() {
  return (
    <div>
      <Head>
        <title>Ismael Alcalá</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className={"container-fluid"}>
          <div className={"row"}>
            <div className={`col-md-8 ${styles.inicio}`}>
              <div className={"row"}>
                <div className={"col-md-12"} align="center">
                  <img
                    src="/img/perfil.jpg"
                    alt="imagen de perfil"
                    className={styles.perfil}
                  />
                </div>
              </div>
              <br></br>
              <div className={"row"}>
                <div className={"col-md-12"} align="center">
                  <Frase></Frase>
                </div>
              </div>
              <div className={"row"}>
                <div className={"col-md-4"}></div>
                <div className={"col-md-4"} align="center">
                  <hr></hr>
                  <div className={"row"}>
                    <div className={"col-md-3"}></div>
                    <div className={"col-md-2"}>
                      <a href="https://web.facebook.com/ismael.alcala.18/">
                        <FontAwesomeIcon
                          icon={faFacebook}
                          size={"lg"}
                          style={{ color: "#a4b0be" }}
                        />
                      </a>
                    </div>
                    <div className={"col-md-2"}>
                      <a href="https://www.instagram.com/ismael.alcala21/?hl=es">
                        <FontAwesomeIcon
                          icon={faInstagram}
                          size={"lg"}
                          style={{ color: "#a4b0be" }}
                        />
                      </a>
                    </div>

                    <div className={"col-md-2"}>
                      <a href="https://www.youtube.com/channel/UCNOII4BeYN8CDmqha8fFRSQ">
                        <FontAwesomeIcon
                          icon={faYoutube}
                          size={"lg"}
                          style={{ color: "#a4b0be" }}
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <br></br>
            </div>

            <div className={`col-md-4 ${styles.soy}`}>
              <div className={"row"}>
                <div className={`col-md-12 ${styles.quien}`} align="center">
                  <h2>¿Quien soy?</h2>
                  <br></br>
                  <p>
                    ¡Hola!, soy Ismael un desarrollador Full Stack apasionado
                    por el código.
                  </p>
                  <p>
                    Estudiante de Ingeniería en Computación en la Universidad
                    Autónoma del Estado de México y con experiencia en el
                    desarrollo de software de más por mas de 4 años
                  </p>
                </div>
              </div>
            </div>
          </div>
          <br></br>

          <div className={"row"}>
            <div className={"col-md-1"}></div>
            <div className={"col-md-5"}>
              <div className={"row"}>
                <div className={"col-md-12"} align="center">
                  <h3>Experiencia</h3>
                </div>
                <div className={"col-md-12"} align="center">
                  <p>
                    Me gusta emprender nuevos retos y aprender por mi cuenta,
                    con lo cual he ganado una amplia experiencia desarrollando
                    aplicaciones móviles, web y de escritorio. Durante mi
                    proceso de aprendizaje, he utilizado diferentes metodologías
                    de desarrollo, entre ellas, el marco de trabajo Agile{" "}
                    <b>SCRUM</b>, con el cual he trabajado por más de un año y
                    medio.
                    <br></br>
                    <br></br>
                    Así mismo he dado <b>mantenimiento</b> a código escrito por
                    terceros, esto con la finalidad de poder crear nuevas
                    versiones de este.
                  </p>
                </div>
              </div>
            </div>
            <div className={"col-md-1"}></div>
            <div className={"col-md-4"}>
              <div className={"row"}>
                <div className={"col-md-12"} align="center">
                  <h3> Habilidaddes</h3>
                </div>
                <div className={"col-md-12"} align="center">
                  <Skill nombre="Laravel" valor="85%"></Skill>
                  <Skill nombre="Vue JS" valor="85%"></Skill>
                  <Skill nombre="React Native" valor="70%"></Skill>
                  <Skill nombre="Angular" valor="60%"></Skill>
                  <Skill nombre="Ionic" valor="65%"></Skill>
                  <Skill nombre="Java" valor="85%"></Skill>
                  <Skill nombre="CSS" valor="70%"></Skill>
                  <Skill nombre="GCP" valor="40%"></Skill>
                </div>
              </div>
            </div>
          </div>
          <br></br>

          <div className={"row"}>
            <div className={"col-md-12"} align="center">
              <h2>Proyectos</h2>
            </div>
            <div className={"col-md-12"} align="center">
              <Proyectos></Proyectos>
            </div>
          </div>
          <br></br>
        </div>
      </main>

      <Pie></Pie>
    </div>
  );
}

import Proyecto from "../components/proyecto";
import styles from "../styles/Proyecto.module.css";

const Proyectos = (props) => {
  return <div className={"row"}>{proyectosRender}</div>;
};

export default Proyectos;
const proyectos = [
  {
    nombre: "SIGAT",
    descripcion: `Este proyecto, está formado por tres componentes principales 
        (Un api, una web app y un agente), los cuales se comunican entre sí,
         para llevar el control de activos de la UAEM (Laravel - Java - Vue Js)`,
    fecha: "2019-2021",
    ver: false,
  },
  {
    nombre: "ResApp",
    descripcion: `Durante el desarrollo de este proyecto de creo una aplicación móvil  hibrida, 
      que permitiera puntuar, comentar y compartir restaurantes cerca, con la finalidad, de poder
       elegir la mejor zona para comer, según las opiniones de los usuarios (React native)`,
    fecha: "2019-2020",
    ver: true,
    link: "https://gitlab.com/ismalcper/rest.git",
  },
  {
    nombre: "TuReporte",
    descripcion: `Se creo una aplicación móvil nativa, para el sistema operativo Android, la cual permitía reportar
    casos de contaminación ambiental (Kotlin)`,
    fecha: "2019-2019",
    ver: false,
  },
  {
    nombre: "SofDeppot",
    descripcion: `Se solicito el desarrollo de un módulo administrativo,
       para el control de combustibles de la empresa NOVUM (Laravel)`,
    fecha: "2021-2021",
    ver: false,
  },
  {
    nombre: "TuPan",
    descripcion: `Creación de una Web App, para llevar el control de inventario y mermas de una panadería local (Laravel)`,
    fecha: "2019-2020",
    ver: true,
    link: "https://gitlab.com/ismalcper/karsapan.git",
  },
  {
    nombre: "TransportesLam",
    descripcion: `Creación de una Web App, para llevar el control y tracking, de los planes de carga de Consultoría  Lam (Laravel)`,
    fecha: "2021-2021",
    ver: false,
  },
  {
    nombre: "CareSave",
    descripcion: `Aplicación móvil  hibrida desarrollada en Ionic para el control de un dispositivo del Internet de las cosas (Ionic)`,
    fecha: "2021-2021",
    ver: true,
    link: "https://gitlab.com/ismalcper/tiposso.git",
  },
  {
    nombre: "FlynsArcade",
    descripcion: `Aplicación web  desarrollada en Java para el entretemiento (mini juegos - Java)`,
    fecha: "2020-2020",
    ver: true,
    link: "https://gitlab.com/david_antunezb/flynsarcade.git",
  },
];
const proyectosRender = proyectos.map((proyecto, index) => (
  <div className={`col-md-4 ${styles.quien}`} align="center" key={index}>
    <Proyecto
      nombre={proyecto.nombre}
      descripcion={proyecto.descripcion}
      fecha={proyecto.fecha}
      ver={proyecto.ver}
      link={proyecto.link}
    ></Proyecto>
  </div>
));

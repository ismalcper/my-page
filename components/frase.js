import { useState } from "react";

const Frase = () => {
  const [count, setCount] = useState(Math.floor(Math.random() * frases.length));

  return (
    <div>
      <p>
        <b>{frases[count]}</b>
      </p>
    </div>
  );
};
const frases = [
  "¡Juega con el código! y resuelve tu vida",
  "¡No vivas!, diviértete y aprende",
  "Eres el punto y coma de mi código",
  "No pienses, crea, y transforma el mundo",
  "Una linea de codigo, simpre sera la solucion... o el problema",
];
export default Frase;

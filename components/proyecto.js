import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolderOpen } from "@fortawesome/free-regular-svg-icons";
const Proyecto = (props) => {
  return (
    <div className="card" style={mystyle}>
      <div className="card-body">
        <h4 className="card-title">
          {props.nombre + " "}

          {props.ver && (
            <a href={props.link} target="_blank">
              ({"Ver codigo "}
              <FontAwesomeIcon
                icon={faFolderOpen}
                size={"md"}
                style={{ color: "white" }}
              />
              )
            </a>
          )}
        </h4>

        <small>({props.fecha})</small>
        <p className="card-text">{props.descripcion}</p>
      </div>
    </div>
  );
};

export default Proyecto;

const mystyle = {
  color: "white",
  backgroundColor: "#ffa502",
  padding: "10px",
  fontFamily: "Arial",
};

const Skill = (props) => {
  return (
    <div>
      <div className={"row "}>
        <div align="left">
          <label>
            
            <b>{props.nombre}</b>
          </label>
          <div className={"progress"}>
            <div
              className={"progress-bar bg-warning progress-bar-animated"}
              role="progressbar"
              style={{ width: props.valor }}
              aria-valuemin="0"
              aria-valuemax="100"
            >
              {props.valor}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skill;
